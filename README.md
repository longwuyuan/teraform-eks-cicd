# Overview

* The design is to use terraform for 4 components.
    (1) A Foundation to use terraform with a backend instead of a local-file
    (2) An AWS VPC
    (3) An AWS EKS Cluster, a Kubernetes platform on top of the AWS infrastructure
    (4) The Kubernetes manifests that declare kubernetes objects & their states

* This solution first creates a AWS S3 bucket and some AWS Dynamodb tables.
* The terraform statefiles persists on the s3 bucket instead of on a host/container/laptop/desktop etc.
* Gitlab-CI is the chosen CI/CD tool so all the work is done by gitlab-ci pipelines.
* The gitlab-ci pipeline will access and maintain the terraform state on the s3 backend, each time it runs.
* An AWS VPC is created .
* A EKS cluster is instantiated with the above mentioned VPC as underlying infrastructure.
* The gitlab-ci jobs use terraform & kubernetes tools to maintian the EKS cluster, including autoscaling of resources.
* The gitlab-ci pipelin uses terraform & kubernetes tools to orchestrate high availability of the dummygoapp application.
* The application is deployed as containers in kubernetes orchestration.

# Requirements

* A gitlab account
* This code as a project in gitlab
* AWS IAM Account 
* For the above AWS IAM account, permissions to create+manage VPC & EKS and associates services
* Variables in the settings CI/CD section of the gitlab project
    - AWS-IAM credentials ;
        - access-key
        - secret-key
        - region
    - Gitlab credentials ;
        - login username
        - password or token

# Description

This is a CI & CD pipeline for a "helloworld" golang application.
The SCM has to be gitlab for this code to work.
The file .gitlab-ci.yaml in the root of the gitlab project is required.
The file .gitlab-ci.yml causes gitlab to automatically trigger the CI/CD pipeline.

## CI/CD Pipeline Stages

This solution is implemnted in 4 stages of the gitlab-ci pipeline ;
All stages run terraform software in a alpine linux container.
All stages get the required tooling installed and the required environment configured to work with multiple components of the design.
Very few GITLAB's inbuilt variables are used.
AWS & Gitlab credential variables, from gitlab project settings, are passed to the container environment.
Gitlab has a features to configure variables that are masked in the build logs.
The gitlab project settings is configured with the accesskey, secretkey and region variables.
The lightweight terraform container is instantiated with environment variables for aws authentication.
The values for the variables that terraform ingests are set from the gitlab project variables.
Each stage runs with its own terraform config. Reasons are explained below.

### Terraform-Backend Stage

* Since no backend exists to begin with, the pipeline creates s3 bucket and dynamodb table.
* S3 bucket is for storing the teraform state.
* Dynamodb table is for managing locking on the terraform statefile.
* Using a backend to store tfstate file is required because the ci jobs are run in ephemeral environs.
* Epheral environs can not provide persisting filesystem or locking, for the tfstate file.
* Code for managing the terraform backend also is included in the solution.
* Code for managing terraform backend also is also written in terraform as the entire solution is terraform based.
* A lightweight, alpine linux based official image of terraform executable, by hashicorp, is used by the gitlab-ci.
* Less coding is needed if we use awscli to create s3 bucket and dynamodb table.
* But terraform is used since there would be consistency and granularity in line with infrastructure as code approach.
* A manual job is configured to destroy the terraform backend. But this is too intrusive so design can be improved & secured.

### VPC Stage

* There is hell go through if you hit dependency problems with terraform.
* Several dependencies are implied between resources and terraform understands them.
* There are also use-cases where the dependencies are not obvious.
* Its ok to deal with unimplied and unknown dependencies on a interactive shell on a laptop.
* But dependency issues cause the CI pipelines to fail. This is unacceptable.
* The EKS module breaks the CI pipeline if run in the same terraform config as the VPC.
* So VPC and EKS are being created in 2 seperate terraform configs.
* Terraform init occurs with a backend configures to store the statefile on s3.
* The popular terraform-aws-module/vpc/aws is invoked and a vpc is created.
* Terraform is configured to make the vpcid available via output variable.
* The pipeline writes the vpcid to a text file terraform.tfvars.
* The pipeline copies the terraform.tfvars to the s3 bucket.
* There are other outputs like nat-ip-address and subnet-ids that are output by vpc module.

### EKS Stage

* EKS is the defacto PaaS for kubernetes, if the provider is AWS.
* So the app runs on EKS.
* EKS is deployed on a VPC, for production ready use.
* EKS is created using github's popular terraform-aws-modules/eks/.
* This is akin to the the vpc module, that is pre-included in the challenge zip.
* The terraform.tfvars file created by the VPC stage is downloaded from S3.
* The vpcid is declared as a variable and the value from the tfvars files is ingested by the EKS config of terraform init.
* The vpc id is used as part of datasource and the subnet_ids of the VPC obtained from it.
* The vpcid and the subnet-ids are the input data for the EKS module to work with dependency problems.
* EKS creation has 2 critical outputs.
    - The kubeconfig file of the EKS cluster
    - A configmap to authenticate the cluster nodes joining the cluster
* The EKS generated kubeconfig file & the configmap file are copied off to the s3 bucket.
* The EKS generated config-map is basically needed to allow nodes to join the cluster.
* Additional installation of other needed tools is done on the container.
    - Py3-Pip is installed to get awscli from pip.
    - kubectl is install to all things kubernetes.
    - aws-iam-authenticator is installed to make kubectl work with the generated kubeconfig.
    - curl is installed to download kubectl as per oficial docs
    - The executables are copied to /root/bin on the container.
    - The CI container is configured with env vars and path as needed.
* The main.tf of eks stage first configures the s3 backend described above.
* The lock is configured in a seperate the dynamodb table.
* The EKS module creates the cluster and produces 2 artifacts.
* The EKS modules produces a kubeconfig file and also a configmap.
* The kubeconfig and the configmap are safely copied to the s3 bucket in use.
* A directory /root/.kube is created and the kubeconfig is copied there for ease.
* EKS generated kubeconfig is copied to /root/.kube/config
* The aws-iam-authneticator verifies the aws credentials
* Kubectl applies the configmap generated by the EKS module
* The role arn in the configmap reconfigures auth for the worker nodes EC2 instances.
* Nodes join the EKS cluster and change to ready state.
* Now we are ready to manage objects in the EKS cluster.

### K8S Stage

* K8s stage terraform inits with its own locking and backend.
* The tf files declare the terraform env config as well as the manifests.
* The lightweight container used has terraform, curl, aws-iam-authenticator, awscli installed.
* Terraform files create and/or change secrets, deployments, pods & services on EKS.


## Application lifecycle management & rollback

* There are multiple techniques and choices for a rollback method
* The choice of a rollback process would depend on the workflow and use-case of an engineering team.
* Kubernetess has inbuilt rolling updates and related rollback.
* Gitlab-Autodevops implements a concept of "review environs".
* Review environs are where a git checkin creates a build and deploys the build to a cluster.
* But the automation and instrumentation of the CI/CD pipeline allows to run such a build without a code merge.
* So the developers get a choice to merge-code after reviewing the new builds from their checkins.
* Review environs are offered by Jenkin-X as well.
* There is anohter practice of tagging the builds with latest and stable like words.
* In this use case, latest becomes the candiddate for deployment and stable becomes the candidate to rollback to.
* Personally, I would choose a tool and process that best suits the project and the workflow I am working on.
* Using unix ideology that one tool does one job well, argocd is my choice for, CD and gitlab is my choice for CI.

## Further improvements

* Since the performance, reliability, security and cost-effectiveness of different ways has not been evaluated, i would first compare the following approaches ;

    - Versioning the app is not implemented yet so that has to be done
    - Terraform for EKS with kubectl kustomize for manifests
    - AWS cli for EKS with kubectl kustomize for manifests
    - Terraform for EKS with helm for manifests
    - AWS cli for EKS with helm for manifests
    - Rancher.com with HELM
    - Rancher.com with Kustomize & ArgoCD
