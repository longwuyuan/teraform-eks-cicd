# Create a S3 bucket to store the tfstate 
resource "aws_s3_bucket" "tfstate-s3-bucket" {
  bucket = "s3-bucket-0"
  versioning {
    enabled = true
  }
  lifecycle {
    prevent_destroy = false
  }
  tags = {
    Name = "s3 bucket 0"
  }
}

# Create a dynamodb table for locking the vpc state file
resource "aws_dynamodb_table" "dynamodb-vpc-tfstatelock" {
  name           = "vpc-tfstatelock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "vpc tfstatelock table"
  }
}

# Create a dynamodb table for locking the eks state file
resource "aws_dynamodb_table" "dynamodb-eks-tfstatelock" {
  name           = "eks-tfstatelock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "eks tfstate lock table"
  }
}

# Create a dynamodb table for locking the deployment state file
resource "aws_dynamodb_table" "dynamodb-k8s-tfstatelock" {
  name           = "k8s-tfstatelock"
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20
  attribute {
    name = "LockID"
    type = "S"
  }
  tags = {
    Name = "k8s tfstate lock table"
  }
}
