# Configuring AWS s3 bucket to store tfstate
# Configuring a dynamodb table to enable locks on the statefile
terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "s3-bucket-0"
    dynamodb_table = "k8s-tfstatelock"
    region         = "eu-central-1"
    key            = "tfstates/k8s/k8s.tfstate"
  }
}

provider "kubernetes" {}

# Secret to download image from gitlab private registry
resource "kubernetes_secret" "gitlab-readreg-token" {
  metadata {
    name = "gitlab-readreg-token"
  }
  data = {
    ".dockerconfigjson" = "${file("/root/.docker/config.json")}"
  }
  type = "kubernetes.io/dockerconfigjson"
}


# K8s deployment resource
resource "kubernetes_deployment" "app-0" {
  metadata {
    name = "app-0"
    labels = {
      appname = "app-0"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        appname = "app-0"
      }
    }
    template {
      metadata {
        labels = {
          appname = "app-0"
        }
      }
      spec {
        container {
          image = var.app-0_image
          name  = "app-0"
          port {
            container_port = 80
          }
          resources {
            limits {
              cpu    = "0.5"
              memory = "50Mi"
            }
            requests {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
        image_pull_secrets {
          name = "gitlab-readreg-token"
        }
      }
    }
  }
}

# Loadbalancer service for the app 
resource "kubernetes_service" "app-0" {
  metadata {
    name = "app-0"
  }
  spec {
    selector = {
      appname = kubernetes_deployment.app-0.metadata[0].labels.appname
    }
    port {
      port        = 80
      target_port = 8080
    }
    type = "LoadBalancer"
  }
}
