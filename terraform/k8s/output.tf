# Output the lb service hostname
data "kubernetes_service" "app-0" {
  metadata {
    name = "app-0"
  }
}

output "lb_hostname" {
  value = data.kubernetes_service.app-0.load_balancer_ingress
}
