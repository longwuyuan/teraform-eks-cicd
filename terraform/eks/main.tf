# AWS provider declared
# Creds from env vars in gitlab settings are passed to the ci container
provider "aws" {}

# Configuring aws s3 bucket to store statefile
# Configuring a dynamodb table to enable locks on the statefile, for multiuser editing of state
terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "s3-bucket-0"
    dynamodb_table = "eks-tfstatelock"
    region         = "eu-central-1"
    key            = "tfstates/eks/eks.tfstate"
  }
}

# Get the subnet ids with a data source
data "aws_subnet_ids" "subnet_ids" {
  vpc_id = var.vpc_id
}

# Creating a eks cluster
module "eks-cluster" {
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "eks-cluster-0"
  subnets      = data.aws_subnet_ids.subnet_ids.ids
  vpc_id       = var.vpc_id

  worker_groups = [
    {
      instance_type = "t2.small"
      asg_max_size  = 3
      tags = [{
        key                 = "type"
        value               = "eksworker"
        propagate_at_launch = true
      }]
    }
  ]

  tags = {
    environment = "eks cluster 0"
  }
}
