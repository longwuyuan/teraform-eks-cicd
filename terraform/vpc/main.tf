# AWS provider creds from env vars in gitlab settings passed to ci container
provider "aws" {}

# Configuring aws s3 backend to store terraform statefile
# Configuring a dynamodb table to enable locking on the statefile
terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "s3-bucket-0"
    dynamodb_table = "vpc-tfstatelock"
    region         = "eu-central-1"
    key            = "tfstates/vpc/vpc.tfstate"
  }
}

# Creating a VPC
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-0"
  cidr = var.cidr

  azs             = var.availability_zones
  private_subnets = var.private_subnets_cidr
  public_subnets  = var.public_subnets_cidr

  enable_nat_gateway = true
  enable_vpn_gateway = true

  tags = {
    Context = "tf-aws-vpc-eks-k8s"
  }
}
